from django.urls import path
from . import views

urlpatterns = [
    path('show_sign_up_page/', views.show_sign_up_page,name="show_sign_up_page"),
    path('show_sign_in_page/', views.show_sign_in_page,name="show_sign_in_page"),
    path('show_home_page/', views.show_home_page,name="show_home_page"),
    path('show_otp_page/', views.show_otp_page,name="show_otp_page"),
    path('show_forgot_password_page/', views.show_forgot_password_page,name="show_forgot_password_page"),

    path('sign_up/', views.sign_up,name="sign_up"),
    path('sign_in/', views.sign_in,name="sign_in"),
    path('change_password/', views.change_password,name="change_password"),

    path('deposit_post/', views.deposit_post,name="deposit_post"),
    
    
]