from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class UserProfile(models.Model):
    user = models.ForeignKey(
            User,on_delete=models.CASCADE,null=True,blank=True
            )
    image = models.ImageField(
            upload_to = "profile_image",null=True,blank=True
            )
    phone = models.CharField(max_length=20,null=True,blank=True)
    address = models.CharField(max_length=255,null=True,blank=True)   
    city = models.CharField(max_length=100,null=True,blank=True)
    status = models.BooleanField(default=False,null=True,blank=True)


class Deposit(models.Model):
    success_details = (
        ('Processing','Processing'),
        ('Success','Success'),
        ('Failed',"Failed")
        )
    user = models.ForeignKey(
            User,on_delete=models.CASCADE,null=True,blank=True
            )
    image = models.ImageField(
            upload_to = "Deposit_image",null=True,blank=True
            )
    amount = models.CharField(max_length=20,null=True,blank=True)
    date = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    mode_of_payment = models.CharField(max_length=100,null=True,blank=True)
    status = models.CharField(choices=success_details,max_length=100,null=True,blank=True,default="Processing")
    status_mode = models.BooleanField(default=False,null=True,blank=True)


class Withdraw(models.Model):
    success_details = (
        ('Processing','Processing'),
        ('Success','Success'),
        ('Failed',"Failed")
        )
    user = models.ForeignKey(
            User,on_delete=models.CASCADE,null=True,blank=True
            )
    image = models.ImageField(
            upload_to = "Withdraw_image",null=True,blank=True
            )
    amount = models.CharField(max_length=20,null=True,blank=True)
    date = models.DateTimeField(auto_now_add=True,null=True,blank=True)
    mode_of_payment = models.CharField(max_length=100,null=True,blank=True)
    amount_credited = models.BooleanField(default=False,null=True,blank=True)
    status = models.CharField(choices=success_details,max_length=100,null=True,blank=True)
    status_mode = models.BooleanField(default=False,null=True,blank=True)


class BankDetails(models.Model):
    user = models.ForeignKey(
            User,on_delete=models.CASCADE,null=True,blank=True
            )
    name = models.CharField(max_length=100,null=True,blank=True)
    account_number = models.CharField(max_length=100,null=True,blank=True)
    ifsc = models.CharField(max_length=100,null=True,blank=True)
    bank_name = models.CharField(max_length=100,null=True,blank=True)
    branch = models.CharField(max_length=100,null=True,blank=True)
    city = models.CharField(max_length=100,null=True,blank=True)
    status_mode = models.BooleanField(default=False,null=True,blank=True)