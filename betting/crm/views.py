# Python Imports
import json

# Django Imports
from django.shortcuts import render
from django.http import JsonResponse,HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate


# Rest Framework Imports
from rest_framework.decorators import api_view


# CRM Imports
from .models import *


# Others Imports


# Create your views here.

@api_view(['POST','GET','PUT','DELETE'])
def show_sign_up_page(request):
    if request.method == "GET":
        return render(request,'signup.html')


@api_view(['POST','GET','PUT','DELETE'])
def show_sign_in_page(request):
    if request.method == "GET":
        return render(request,'signin.html')


@api_view(['POST','GET','PUT','DELETE'])
def show_home_page(request):
    if request.method == "GET":
        str1 = None
        str2 = None
        str3 = None
        name = request.data.get("username")
        return render(request,'homepage.html',context={'name':name,'str1':str1,'str2':str2,'str3':str3})

@api_view(['POST','GET','PUT','DELETE'])
def show_otp_page(request):
    if request.method == "GET":
        return render(request,'otp.html')


@api_view(['POST','GET','PUT','DELETE'])
def show_forgot_password_page(request):
    if request.method == "GET":
        return render(request,'forgotpassword.html')


@api_view(['POST','GET','PUT','DELETE'])
def sign_up(request):
    if request.method == "POST":
        username = request.data.get("username")
        email = request.data.get("email")
        password = request.data.get("password")
        re_password = request.data.get("re_password")
        address = request.data.get("address")
        phone = request.data.get("phone")
        city = request.data.get("city")
        image = request.FILES.get('image')

        if re_password == password:
            if User.objects.filter(username=username).exists():
                return JsonResponse({'success':False,'data':'Username Already Exists'})
            if User.objects.filter(email=email).exists():
                b = User.objects.filter(email=email)
                print(len(b),"bbbbb")
                return JsonResponse({'success':False,'data':'Email Already Exists'})
            else:
                user = User.objects.create(
                        username=username,
                        email=email,
                        password=make_password(password)
                        )  
                user_profile = UserProfile.objects.create(
                        user_id=user.id,
                        image=image,
                        phone=phone,
                        address=address,
                        city=city
                        ) 
                print(user_profile,"user")
                print(user.id,"user id")
                return JsonResponse({'success':True,'data':'Successfully Registered'})
        else:
            return JsonResponse({'success':False,'data':'Password Does not Matched'})


@api_view(['POST','GET','PUT','DELETE'])
def sign_in(request):
    if request.method == "POST":
        username = request.data.get('username')
        password = request.data.get("password")
    
        try:
            data = User.objects.get(email=username)
            if data is not None:
                user = authenticate(username=data.username,password=password)
                if user is not None:
                    return JsonResponse({'success':True,'data':'Successfully Logged In'})
                else:
                    return JsonResponse({'success':False,'data':'Username or password is incorrect'})        
            
        except:
            user = authenticate(username=username,password=password)
            if user is not None:
                return JsonResponse({'success':True,'data':'Successfully Logged In'})
            else:
                return JsonResponse({'success':False,'data':' username or Password is incorrect'})
            return JsonResponse({'success':False,'data':'Password is incorrect'})


@api_view(['POST','GET','PUT','DELETE'])
def change_password(request):
    if request.method == "POST":
        username = request.data.get("username")
        password = request.data.get("password")
        re_password = request.data.get("re_password")
        if re_password == password:
            data = User.objects.get(username=username)
            if data is not None:
                data_qs = User.objects.filter(id=data.id).update(password=make_password(password))
                return JsonResponse({'success':True,'data':'Password Changed successfully !'})
        else:
            return JsonResponse({'success':False,'data':'Password Does not Matched'})

        
@api_view(['POST','GET','PUT','DELETE'])
def deposit_post(request):
    if request.method == "POST":
        user = request.user
        image = request.FILES.get("image")
        amount = request.data.get("amount")
        mode_of_payment = request.data.get("mode_of_payment")

        if user is not None:
            if image is not  None:
                deposit = Deposit.objects.create(
                        user_id=user.id,
                        amount=amount,
                        mode_of_payment=mode_of_payment,
                        image=image,
                        )
                return JsonResponse({'success':True,'data':'Deposit Successfully, Amount Will be add in 10 Minutes'})
            else:
                return JsonResponse({'success':False,'data':'Please Attach image'})
        else:
            return JsonResponse({'success':False,"data":'You need to login'})
